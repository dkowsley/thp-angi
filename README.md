# thp-angi



## Setup

Make sure you have `make` installed, and run the following.
```bash
make kustomize operator-sdk helm-operator opm
```

## Build Bundle

```bash
make bundle
```

## Run Tests

After your bundle has been built, run the following tests to verify package is stable.
```bash
operator-sdk scorecard bundle -o text
```

## Deploy Operator

Preferred method for now is to run locally built images (as this does not contain multi-arch builds at present)
```bash
make docker-build run
```

## Deploy PodInfo Stack

```bash
kubectl apply -f config/samples/charts_v1alpha1_podinfo.yaml
```

## Connect to PodInfo Stack

```bash
kubectl port-foward svc/podinfo-sample http
```